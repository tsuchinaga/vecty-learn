package main

import (
	"github.com/gopherjs/vecty"
	"gitlab.com/tsuchinaga/vecty-learn/store"
	"gitlab.com/tsuchinaga/vecty-learn/views"
)

func main() {
	vecty.SetTitle("べくてぃーの練習")

	v := new(views.View)
	store.Listener.Add(func() {
		vecty.Rerender(v)
	})

	vecty.RenderBody(v)
}
