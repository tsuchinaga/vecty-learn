package views

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"gitlab.com/tsuchinaga/vecty-learn/components"
)

type View struct {
	vecty.Core
}

func (v *View) Render() vecty.ComponentOrHTML {
	return elem.Body(
		elem.Div(vecty.Text("こんにちわーるど")),
		new(components.GeoLocation),
		new(components.AddItem),
		new(components.ItemList),
	)
}
