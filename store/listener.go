package store

type listener struct {
	listener []func()
}

func newListener() *listener {
	return &listener{make([]func(), 0)}
}

func (l *listener) Add(f func()) {
	l.listener = append(l.listener, f)
}

func (l *listener) fire() {
	for _, f := range l.listener {
		f()
	}
}
