package store

import (
	"encoding/json"
	"gitlab.com/tsuchinaga/vecty-learn/models"
	"syscall/js"
)

func init() {
	attachLocalStorage()

	if data := js.Global().Get("localStorage").Get("items"); data != js.Undefined() {
		items := make([]*models.Item, 0)
		if err := json.Unmarshal([]byte(data.String()), &items); err != nil {
			panic(err)
		}
		Items = items
	}
}

func attachLocalStorage() {
	Listener.Add(func() {
		jsonByteItems, err := json.Marshal(Items)
		if err != nil {
			panic(err)
		}

		js.Global().Get("localStorage").Set("items", string(jsonByteItems))
	})
}
