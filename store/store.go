package store

import (
	"gitlab.com/tsuchinaga/vecty-learn/actions"
	"gitlab.com/tsuchinaga/vecty-learn/dispatcher"
	"gitlab.com/tsuchinaga/vecty-learn/models"
)

var (
	Items []*models.Item

	Listener = newListener()
)

func init() {
	dispatcher.Register(onAction)
}

func onAction(action actions.Action) {
	switch a := action.(type) {
	case actions.AddItem:
		Items = append(Items, &models.Item{Title: a.Title})
	case actions.RemoveItem:
		copy(Items[a.Index:], Items[a.Index+1:])
		Items = Items[:len(Items)-1]
	default:
		return
	}

	Listener.fire()
}
