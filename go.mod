module gitlab.com/tsuchinaga/vecty-learn

go 1.12

require (
	github.com/gopherjs/vecty v0.0.0-20190701174234-2b6fc20f8913
	github.com/shurcooL/go v0.0.0-20190704215121-7189cc372560 // indirect
	github.com/shurcooL/go-goon v0.0.0-20170922171312-37c2f522c041 // indirect
)
