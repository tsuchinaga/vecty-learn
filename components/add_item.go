package components

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"github.com/gopherjs/vecty/event"
	"github.com/gopherjs/vecty/prop"
	"gitlab.com/tsuchinaga/vecty-learn/actions"
	"gitlab.com/tsuchinaga/vecty-learn/dispatcher"
)

type AddItem struct {
	vecty.Core
	Title string
}

func (c *AddItem) Render() vecty.ComponentOrHTML {
	return elem.Div(
		elem.Input(
			vecty.Markup(
				prop.Type(prop.TypeText),
				prop.Value(c.Title),
				prop.Placeholder("Title"),
				event.Input(func(e *vecty.Event) {
					c.Title = e.Target.Get("value").String()
				}),
			),
		),
		elem.Button(
			vecty.Markup(event.Click(func(_ *vecty.Event) {
				dispatcher.Dispatch(actions.AddItem{Title: c.Title})
				c.Title = ""
			})),
			vecty.Text("Add"),
		),
	)
}
