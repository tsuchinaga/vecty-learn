package components

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"gitlab.com/tsuchinaga/vecty-learn/store"
)

type ItemList struct {
	vecty.Core
}

func (c *ItemList) Render() vecty.ComponentOrHTML {
	var items vecty.List

	for i, item := range store.Items {
		items = append(items, &ItemRecord{Index: i, Title: item.Title})
	}

	return elem.Div(elem.UnorderedList(items))
}
