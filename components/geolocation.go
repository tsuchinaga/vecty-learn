package components

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"github.com/gopherjs/vecty/event"
	"github.com/gopherjs/vecty/prop"
	"strconv"
	"syscall/js"
)

type GeoLocation struct {
	vecty.Core
	Latitude  float64 // 緯度
	Longitude float64 // 経度
}

func (c *GeoLocation) Render() vecty.ComponentOrHTML {
	return elem.Div(
		elem.Input(vecty.Markup(
			prop.Type(prop.TypeText),
			prop.Value(strconv.FormatFloat(c.Latitude, 'f', 7, 64)),
			vecty.Attribute("readonly", true))),
		elem.Input(vecty.Markup(
			prop.Type(prop.TypeText),
			prop.Value(strconv.FormatFloat(c.Longitude, 'f', 7, 64)),
			vecty.Attribute("readonly", true))),
		elem.Button(vecty.Markup(event.Click(func(_ *vecty.Event) {
			js.Global().Get("navigator").Get("geolocation").Call("getCurrentPosition",
				js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
					coords := args[0].Get("coords")
					c.Latitude, _ = strconv.ParseFloat(coords.Get("latitude").String(), 64)
					c.Longitude, _ = strconv.ParseFloat(coords.Get("longitude").String(), 64)
					vecty.Rerender(c)
					return nil
				}),
				js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
					message := args[0].Get("message").String()
					js.Global().Call("alert", "位置情報の取得に失敗しました: "+message)
					return nil
				}))
		})), vecty.Text("位置情報取得")),
	)
}
