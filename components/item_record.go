package components

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"github.com/gopherjs/vecty/event"
	"gitlab.com/tsuchinaga/vecty-learn/actions"
	"gitlab.com/tsuchinaga/vecty-learn/dispatcher"
)

type ItemRecord struct {
	vecty.Core
	Index int    `vecty:"prop"`
	Title string `vecty:"prop"`
}

func (c *ItemRecord) Render() vecty.ComponentOrHTML {
	return elem.ListItem(
		vecty.Text(c.Title),
		elem.Button(
			vecty.Markup(event.Click(func(_ *vecty.Event) {
				dispatcher.Dispatch(actions.RemoveItem{Index: c.Index})
			})),
			vecty.Text("Remove"),
		),
	)
}
