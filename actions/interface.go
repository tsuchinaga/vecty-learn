package actions

type Action interface {
	action()
}

type baseAction struct{}

func (a baseAction) action() {}
