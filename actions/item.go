package actions

type AddItem struct {
	baseAction
	Title string
}

type RemoveItem struct {
	baseAction
	Index int
}
