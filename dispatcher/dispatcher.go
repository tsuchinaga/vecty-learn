package dispatcher

import "gitlab.com/tsuchinaga/vecty-learn/actions"

var (
	callbacks []func(action actions.Action)
)

func Dispatch(action actions.Action) {
	for _, c := range callbacks {
		c(action)
	}
}

func Register(callback func(action actions.Action)) {
	callbacks = append(callbacks, callback)
}
